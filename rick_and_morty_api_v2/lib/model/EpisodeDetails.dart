class EpisodeDetails {
  int id;
  String name;
  String episode;
  List<String> characters;
  String url;

  EpisodeDetails({
    this.id,
    this.name,
    this.episode,
    this.characters,
    this.url
  });

  factory EpisodeDetails.fromJson(Map<String, dynamic> json) {
    return EpisodeDetails(
      id: json['id'],
      name: json['name'],
      episode: json['episode'],
      characters: List<String>.from(json['characters']),
      url: json['url']
    );
  }
}