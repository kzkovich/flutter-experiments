import 'package:flutter/material.dart';
import 'package:rick_and_morty_api_v2/model/Origin.dart';

class Characters {
  int id;
  String name;
  String status;
  String species;
  String gender;
//  Origin origin;
  String imageUrl;
  List<String> episodesUrl;

  Characters({
    this.id,
    this.name,
    this.status,
//    this.origin,
    this.episodesUrl,
    this.gender,
    this.imageUrl,
    this.species
  });

  factory Characters.fromJson(Map<String, dynamic> json) {
    return Characters(
      id: json['id'],
      name: json['name'],
      status: json['status'],
      species: json['species'],
      gender: json['gender'],
//      origin: Origin.fromJson(json['origin']),
      imageUrl: json['image'],
      episodesUrl: (json['episode'] as List).map((e) => e as String).toList(),
    );
  }

}