class Origin {
  String originName;
  String originUrl;

  Origin({this.originName, this.originUrl});

  factory Origin.fromJson(Map<String, dynamic> json) {
    return Origin(
      originName: json["name"] as String,
      originUrl: json["url"] as String
    );
  }
}