import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rick_and_morty_api_v2/activities/CharacterDetailsTable.dart';
import 'package:rick_and_morty_api_v2/model/Characters.dart';
import 'package:http/http.dart' as http;

class RAMCharactersListView extends StatefulWidget {
  _ListViewJsonApiState createState() => _ListViewJsonApiState();
}

class _ListViewJsonApiState extends State<RAMCharactersListView> {
  final String url = 'https://rickandmortyapi.com/api/character/';

  Future <List<Characters>> _fetchCharacters() async {
    var response = await http.get(url);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      Iterable list = result['results'];
      return list.map((model) => Characters.fromJson(model)).toList();
    } else {
      throw Exception('Failed to load Characters');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Characters'),
      ),
      body: FutureBuilder<List<Characters>>(
        future: _fetchCharacters(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Center(child: CircularProgressIndicator());

          return ListView(
          children: snapshot.data
            .map((character) => ListTile(
              title: Text(character.name),
              subtitle: Text(character.gender),
              leading: CircleAvatar(
                radius: 30.0,
                backgroundColor: Colors.transparent,
                child: ClipOval(
                  child: Image.network(character.imageUrl),
                )
              ),
              trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
                Navigator.push(context,
                MaterialPageRoute(
                  builder: (context) => CharacterDetailsTable(character: character)
                ));
            }

            )).toList(),
          );
        }
      )
    );
  }


}