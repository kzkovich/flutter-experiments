import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rick_and_morty_api_v2/model/Characters.dart';

class CharacterDetailsTable extends StatefulWidget {
  final Characters character;

  CharacterDetailsTable({Key key, this.character}) : super(key: key);

  _EpisodeDetailsState createState() => _EpisodeDetailsState();
}

class _EpisodeDetailsState extends State<CharacterDetailsTable> {
  List<String> listOfEpisodeNames;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('${widget.character.name}'),
        ),
        body: Column(
          children: <Widget>[
            FlatButton(
              child: Text('Load Names'),
              onPressed: () async {
                await _runFutures();
              },
            ),
            if (listOfEpisodeNames != null)Expanded(
              child: _populateEpisodes()
            ) else CircularProgressIndicator(),
          ],
        ));
  }

  Widget _populateEpisodes() {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) =>
          Text(listOfEpisodeNames[index]),
      itemCount: listOfEpisodeNames.length,
    );
    return Column(
      children: <Widget>[
        for (var episodeName in listOfEpisodeNames) Text(episodeName)
      ],
    );
  }

  Future<String> _parseEpisodeName(String url) async {
    var response = await http.get(url);
    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      String episodeName = result['name'];
      return episodeName;
    }

    return null;
  }

  Future _runFutures() async {
    listOfEpisodeNames = [];
    for (var url in widget.character.episodesUrl) {
      final episodeNameFromFuture = await _parseEpisodeName(url);
      listOfEpisodeNames.add(episodeNameFromFuture);
    }
    setState(() {});
  }
}
