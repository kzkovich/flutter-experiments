import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rick_and_morty_characters/widgets/characterList.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rick And Morty Characters titleMa',
      home: Scaffold(
        appBar: AppBar(
          title: Text("RAM"),
        ),
        body: Center(
          child: CharactersListState,
        ),
      ),
    );
  }

}