class Constants {
  static final String R_A_M_CHARACTERS = 'https://rickandmortyapi.com/api/character/';
  static final String FIRST_EPISODE = 'https://rickandmortyapi.com/api/episode/1/';
  static final String DEFAULT_AVATAR = 'https://rickandmortyapi.com/api/character/avatar/1.jpeg';
}