import 'dart:convert';

import 'package:rick_and_morty_characters/services/webservice.dart';
import 'package:rick_and_morty_characters/utils/constants.dart';

class EpisodeDetails {
  final int id;
  final String name;
  final String episode;

  EpisodeDetails({this.id, this.name, this.episode});

  factory EpisodeDetails.fromJson(Map<String, dynamic> json) {
    return EpisodeDetails(
        id: json['id'],
        name: json['name'],
        episode: json['episode']
    );
  }

  static Resource<List<EpisodeDetails>> get episodeName {
    return Resource(
        url: Constants.FIRST_EPISODE,
        parse: (response) {
          final result = jsonDecode(response.body);
          Iterable resultIterable = result;
          return resultIterable.map((episodeLink) => EpisodeDetails.fromJson(episodeLink));
        }
    );
  }
}

