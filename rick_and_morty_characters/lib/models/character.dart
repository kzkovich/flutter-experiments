import 'dart:convert';
import 'package:rick_and_morty_characters/services/webservice.dart';
import 'package:rick_and_morty_characters/utils/constants.dart';

class Character {
  final int id;
  final String name;
  final String status;
  final String image;
  final List<String> episodeLinks;

  Character({this.id, this.name, this.status, this.image, this.episodeLinks});

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
        id: json['id'],
        name: json['name'],
        status: json['status'],
        image: json['image'] ?? Constants.DEFAULT_AVATAR,
        episodeLinks: List<String>.from(json['episode'].map((x) => x))
    );
  }

  static Resource<List<Character>> get all {
    return Resource(
        url: Constants.R_A_M_CHARACTERS,
        parse: (response) {
          final result = json.decode(response.body);
          Iterable list = result['results'];
          return list.map((model) => Character.fromJson(model)).toList();
        }
    );
  }
}
  
//  static Resource<List<Character>> get episodeLink {
//    return Resource(
//      url: Constants.FIRST_EPISODE,
//      par
//    )
//  }
//}