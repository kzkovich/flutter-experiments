import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rick_and_morty_characters/models/character.dart';
import 'package:rick_and_morty_characters/models/episodeDetails.dart';

class CharacterDetail extends StatelessWidget {
  final Character character;
  final EpisodeDetails episodeName;

  CharacterDetail({Key key, @required this.character, this.episodeName}) : super (key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(character.name),
      ),
      body: Container(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Image.network(character.image),
                Text(character.name),
                Text(character.status),
                Text(episodeName.name),
              ],
            )
        ),
      ),
    );
  }
}