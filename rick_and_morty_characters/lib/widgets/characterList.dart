import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rick_and_morty_characters/models/character.dart';
import 'package:rick_and_morty_characters/models/episodeDetails.dart';
import 'package:rick_and_morty_characters/services/webservice.dart';
import 'package:rick_and_morty_characters/utils/constants.dart';
import 'package:rick_and_morty_characters/widgets/characterDetail.dart';
import 'package:http/http.dart' as http;

class CharactersListState extends State<CharactersList> {
  List<Character> _characterResult = List<Character>();
  EpisodeDetails _episodeDetails;

  @override
  void initState() {
    super.initState();
    _populateCharacterList2();
  }

  Future<List<Character>> _populateCharacterList2() async {
    final characterApiUrl = Constants.R_A_M_CHARACTERS;
    final response = await http.get(characterApiUrl);

    if (response.statusCode == 200) {
      List jsonResponse = jsonDecode(response.body);
      return jsonResponse.map((character) =>
      new Character.fromJson(character)).toList();
    } else {
      throw Exception("Failed to load data!");
    }
  }
//      final character = await Webservice.load(Character.all);
//      final episodeDetails = await Webservice.load(EpisodeDetails.episodeName);
//      final resource = Resource(
//          url: 'https://rickandmortyapi.com/api/episode/1',
//          parse: (response) {
//            final result = json.decode(response.body);
//            Iterable list = result['results'];
//            return list.map((model) => Character.fromJson(model)).toList();
//          }
//      );
//
//      final character2 = await Webservice.load(resource);
//
//    } catch (e) {} finally {}
//  }

//  _characterResult[index].image == null ? Image.asset(Constants.DEFAULT_AVATAR) : Image.network(_characterResult[index].image),
  ListTile _buildItemsForListView(BuildContext context, int index) {
    return ListTile(
        leading: CircleAvatar(
            child:
                ClipOval(child: Image.network(charac[index].image)),
            backgroundColor: Colors.transparent,
            radius: 30.0),
        title: Text(_characterResult[index].name),
        subtitle: Text(_characterResult[index].status,
            style: TextStyle(fontSize: 12)),
        onTap: () async {
          Webservice.load(Character.all).then((character) => {
            setState(() => {_characterResult = character})
          });
//          Webservice.load(EpisodeDetails.episodeName).then((episodeName) => {
//            setState(() => {_episodeDetails = episodeName})
//          });
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  CharacterDetail(character: _characterResult[index]),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Character>>(
      future: _populateCharacterList2(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Character> character = snapshot.data;
          return _characterListViewBuilder(character);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  ListView _characterListViewBuilder(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return _buildItemsForListView(data[index].)
    }
    )
  }

class CharactersList extends StatefulWidget {
  @override
  createState() => CharactersListState();
}
